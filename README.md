Package for deploying template, documentation, configuration and data of the Fontane Notizbuch Project to eXist.

# build
## standard
`ant`
## light
`ant fastdeploy`
This build target creates a package with the smallest subset of items to deploy in a standard installation. Insteat of a complete publication of all 67 notebooks a single one (C07, `textgrid:16b00`) is added to the list, so the deployment is much faster.
## non standard port
if the eXist-db is available via a non-standard port, the post install script needs to be changed.
# dependencies
see [package description](expath-pkg.xml) for details
- eXist shared
- SADE (Fontane edition)
- textgrid-connect (Fontane edition)

# deployment
make sure the user running eXist-db can write at `/var/lib/textgrid`.
requires valid credentials!
place a file named 'credentials.txt' with the following information in /var/lib/textgrid/fontane/
```
TextGrid_User
TextGrid_Password
Secret
Dokuwiki_User
Dokuwiki_Password
```
User and password used for inital publishing of all data from TextGrid, the secret is used for accessing internal area of the website.
See [post install script](post-install.xql) for details on this process.

A logfile is prepared during the deployment. Most errors are reported in `error` elements at `initlog.xml`.

During the process you can check the logfile or check if xhtml reaches 67:
```
"meta: " || count(xmldb:get-child-resources("/db/sade-projects/textgrid/data/xml/meta/")),
"data: " || count(xmldb:get-child-resources("/db/sade-projects/textgrid/data/xml/data/")),
"tile: " || count(xmldb:get-child-resources("/db/sade-projects/textgrid/data/xml/tile/")),
"xhtm: " || count(xmldb:get-child-collections("/db/sade-projects/textgrid/data/xml/xhtml/")),
//initlog//error
```
this may result in
```
"meta: 222"
"data: 87"
"tile: 132
"xhtm: 67"
```
on complete import (but slightly changes in this numbers are ok as well, as long
  as no error is reported).
