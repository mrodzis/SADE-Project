<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="no"/>
    <xsl:param name="id"/>
    <xsl:param name="link"/>
    <xsl:param name="title"/>
    <xsl:param name="page">
        <xsl:value-of select="/tei:surface/@n"/>
    </xsl:param>
    <xsl:template match="/">
        <div>
            <strong>
                <a href="{$link}&amp;page={$page}">
                    <xsl:value-of select="$title"/> - Blatt <xsl:value-of select="$page"/>
                </a>
            </strong>
        </div>
    </xsl:template>
</xsl:stylesheet>
