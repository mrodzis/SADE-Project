xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=text media-type=text/css omit-xml-declaration=yes";

(for $tei in collection('/db/sade-projects/textgrid/data/xml/data')/tei:TEI
let $nb := substring-after($tei//tei:sourceDoc/@n, 'er_')
return
('#' || $nb || ' .sourceDoc > .surface {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity)||'mm;
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' .sourceDoc > #sspine {
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:depth[1]/@quantity)||'mm;}
',
'#' || $nb || ' .sourceDoc > #souter_back_cover, #'|| $nb || ' .sourceDoc > #souter_front_cover  {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:height[1]/@quantity)||'mm;
    max-height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:height[1]/@quantity)||'mm !important;
    width:' ||string($tei//tei:extent[1]/tei:dimensions[@type = 'binding']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' div.facs {
    height:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity)||'mm;
    width:'||string($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity)||'mm;}
',
'#' || $nb || ' .double {
    width:'||string(number($tei//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity) * 2)||'.5mm;}
'
) (: return :)
) (: for :)
,
"
@font-face {
    font-family: Fontine;
    src: url(/public/fonts/Fontine.woff);
}
@font-face {
    font-family: FreeSans;
    font-strech: extra-condensed;
    src: url(/public/fonts/woff/FreeSans.woff);
}
.doubleHelper{height:150px !important;transition-timing-function: linear;transition: height 1s;}
.section-header{transition-timing-function: linear;transition: height 1s;}

.butthead:before {
  margin-top: -200px;  /* header */
  padding-bottom: 200px;
  display: block;
}

.teixml {
    position: relative;
    display:none;
    /* overflow-y:scroll; < golden Layout */
}
pre {border:none;}

.surface{
    font-family: Fontine;
    font-size:medium;
    line-height: 1cm;
    color: darkslategrey;
    border-color: darkslategrey;
    stroke: darkslategrey;
    fill: darkslategrey;
}
.surface {
    position:relative;
    background: url('/public/img/fontane_empty_bg-cut-bright-compressed-repeat.png') repeat scroll 0 0 papayawhip;
    white-space: nowrap;
}
.surface .surface {
    background-image: none;
    background-color: rgba(255, 239, 213, 0.4);
}
.surface ~ .surface[id$='v'] {
    display: none;
}
.double .surface ~ .surface[id$='v'] {
    display: block;
}
/* show labels */
.surface.teilabel.nested {
    display: block;
}
/* no nested surfaces */
.surface.no-nesting .surface {
    display:none;
}
img.facs {
width: auto;
height: 100%;
}
img[src$='loader.svg'] {
    max-width: 100%;
    max-height: 100%;
    width: 130px;
    height: 30px;
}
/* Doppelseitenansicht */
.double .facs:first-child , .double .surface:first-child {
    float: left;
    z-index:2;
}
.double .facs:nth-child(2) , .double > .surface:nth-child(2) {
    float:right;
}

.Kalenderblatt.verso {
    transform-origin: center center 0;
}

.Kalenderblatt.verso img.facs {
    width: 100%;
    height: 100%;
}
.Kalenderblatt.verso.rotate {
    transform: rotate(180deg);
}
.rowWrapper > .Kalenderblatt {
    left: 0 !important;
}

/* omit second surface on inner_front_cover */
#Ir + #Iv {
    display: none;
}

div.zone.figure > div.zone {
    background-color: rgba(255,255,255,0.5 );
}
.zone.figure { line-height:100%; }
.verticalMiddel {
    vertical-align:middle;
}
.vertical-align.super {
    font-size: 80%;
    vertical-align: super;
}
.vertical-align.sub {
    display: inline-block;
    vertical-align: sub;
    left: 0;
    position: absolute;
    margin-top: 50%;
}
.zone {
    transform-origin: left top;
    -webkit-transform-origin: left top;
}
.zone .zone {
    width:inital;
}

.zoneHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	font-style: italic;
	transform-origin: center center 0;
}
.zone.hover:hover > .zoneHover {display:block}


.line * {
    position: relative;
}

.zone.sum > .item {
    margin-left: inherit !important;
    text-align: right;
}

.addSpan {
  font-size: small;
  line-height: 120%;
}

/* EWZ innerhalb von addSpan */
.addSpan svg {
    top: 0px !important;
    height: 120%;
}

.line.metamark {
    margin-left: 0.5cm;
    position: absolute;
    margin-top: -0.5cm;
}

span.metamark.teicaret.f {
    display: inline-block;
    position: absolute;
    margin-top: -10px;
    margin-left: -5px;
}

.metamarkHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	font-style: italic;
	transform-origin: center center 0;
}
.metamark:hover .metamarkHover {
    display: block;
}

/* EWZ in metamark */
.metamark svg {
    position: absolute;
    height:1.2cm !important;
}
.metamark svg.curved_V {
    top: -24px !important;
}

.metamark .bracket_left {
  display: block;
  transform: scaleX(0.4);
  /* C6 16r in special cases */
  position: absolute;
  bottom: 50%;
  right: 50%;
  left: 50%;
}

.metamark.authorial_note {
    display:block;
    position:absolute;
    bottom:50%;
}

.metamark.cm3 {
  bottom: 1.7cm;
  display: block;
  position: absolute;
/*  transform: scaleX(0.3); */
/* ^ herausgenommen für B3 22r */
}

/* rotation changed: https://projects.gwdg.de/issues/20803 */
.metamark.deletion {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(47,79,79,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px));
    display: inline-block;
}


.metamark.deletion.medium_blue_pencil {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(66,180,230,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px));
    display: inline-block;
}

svg.deletion-points{
  position:absolute;
  top:0;
}

/* Latf ist Standard, siehe .sourceDoc */

.Latn {
    font-family: FreeSans;font-size:0.87em;
}
/* Archivare bekommen Schriftgröße unabhängig von Latn/Latf  */
/* https://projects.gwdg.de/issues/18218 */
.Archivar1.Latf {
    font-size: 0.87em;
}
.Archivar2.Latf {
    font-size: 0.87em;
}
.pencil {
    color: darkslategrey;
    stroke: darkslategrey;
}
.blue_pencil {
    color: #42b4e6;
    fill: #42b4e6;
    stroke: #42b4e6;
    border-color: #42b4e6;
}
.violet_pencil {
    color: #9627f0;
}
.black_ink {
    color:black;
    border-color:black;
    fill:black;
    stroke:black;
}
.brown_ink {
    color: sienna;
    border-color:sienna;
    fill:sienna;
    stroke:sienna;
}
.blue_ink {
    color: blue;
    border-color:blue;
    fill:blue;
    stroke:blue;
}
.light_blue_ink {
    color: #6666FF;
}

.modmedium_pencil > .add > span {
    color: darkslategrey;
}

.handShiftnew {
    content: '\270D';
    position: absolute;
    display: block;
    margin-left: -20px;
}

/* Schriftgröße http://www.eromm.org/dcgkb/doku.php?id=fontane:gesamtdokumentation_iii.3.19 */
.xx-small {
    font-size:xx-small;
}
x-small{
    font-size: x-small;
}
small{
    font-size: small;
}
medium{
    /* nicht codiert */
    font-size: medium;
}
.large{
    font-size: large;
}
.x-large {
    font-size: x-large;
}
.xx-large {
    font-size: xx-large;
}
.font-weight.bold {
    font-weight: bold;
}
.color.red {
    color: red;
}

.retrace {
    position:relative;
}
.retraced {
    position: absolute;
    margin-left: -1px;
    white-space: nowrap;
}
.retraceHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -1cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	transform-origin: center center 0;
}
}
.hoverTop {
    position: absolute;
    top: -100%;
}
.retraced:hover .retraceHover {
    display: block;
}
.italic {
    font-style: italic;
}
.anchored {
    position: absolute;
}
.segHover, .restoreHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	font-style: italic;
	transform-origin: center center 0;
}
.seg:hover .segHover {
    display: block;
}


.choice .expan {
    display:none;
}
.choice:hover .expan {
    display: inline-block;
    position: absolute;
    margin-top: -0.5cm;
    background-color: rgba(100,100,100,0.75);
    color: white;
    padding: 0px 10px 0px 10px;
    white-space: nowrap;
    line-height: 100%;
    font-size: medium;
}
.mod .modHover {display:none;}

.mod:hover .modHover {
	display: inline-block;
	position: absolute;
	margin-top: -1cm;
	background-color: rgba(100, 100, 100, 0.75);
	color: #FFF;
	padding: 0px 10px;
	white-space: nowrap;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	transform-origin: center center 0;
}
.modHover.italic > span {
    font-style: normal !important;
}

.modSeq1.text-decoration.underline.modSeq2 {
  /* only when a modSeq2 is available */
    text-decoration: underline;
}
.modSeq2.text-decoration.underline {
    border-bottom: 1px solid darkslategrey;
    padding-bottom: 1px;
}
/* there are stairs in the seq1 line, see B14 61v */
.modSeq1 {
    padding-bottom: 1px;
}

/* stamps are within tei:zone/tei:seg so, place them absolute to put them on the edge of the zone */
svg {
  position: absolute;
}

/* dont call it label - bootstrap! */
.teilabel > .zone:first-child {
  /* background-color: rgba(210, 210, 210, 0.75); */
    background-image:none;
  /*   height:100%; entfernt siehe A1 outer_front_cover; 9.5.2015  */
    width:100%;
}
/* refine surface background in case of a label */
.surface.teilabel {background:none;}

.sourceDoc > .surface > .surface.teilabel {
    background-color: rgba(100, 100, 100, 0.4);
}
/* ausgesetzt. soll berechnet werden, Hinweis von Adrian betreffend A09 */
/* #outer_front_cover div.surface.teilabel div.zone div.line { */
/*     line-height: 0.7cm !important; */
/* } */

.mod.subst, .mod.transform {
    position: relative;
    display: inline-block;
    text-align: center;
}

.addHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	transform-origin: center center 0;
}
.add:hover > .addHover {display:block}
.addHover + .addHover {
    margin-top: calc(150% + 20px);
}
.addSpanHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	transform-origin: center center 0;
}
.addSpan:hover > .addSpanHover {
    display: block;
    margin-top: -40px;
}
/* .add.above, .add.below { */
/*   font-size:0.75em;*/
/* } */
.add{
    font-size:0.75em;
}

/* ist mod.transform hier noch notwendig oder gar falsch? */
.add.superimposed, .mod.transform > .add {
    left: 0;
    position: absolute;
    width:100%;
}
.add.superimposed {
    font-size: 1em;
}
.add.superimposed.linked {
    font-size: 0.75em;
}
.del.overwritten + .add {
    left: 0;
    position: absolute;
}
.addWrapper {
    display:inline-block;
    /* position:absolute; https://projects.gwdg.de/issues/17815 */
    position:relative;
    height:100%;
    left:0;
    white-space: nowrap;
}
.mod.subst .addWrapper{
    position:absolute;
}
#s2vbr > div:nth-child(2) > div:nth-child(11) > span.mod.subst > span.addWrapper {
    position: relative;
    /* https://projects.gwdg.de/issues/18394 */
}
div#A07.sourceDoc div#s27r.rowWrapper.clearfix.butthead div#27r.surface div.zone div.line span.mod.subst span.addWrapper {
    position: relative;
    top: -0.65cm;
}

.addWrapper .add.above, .addWrapper .add.below {
    display:block;
/*    position:absolute;  https://projects.gwdg.de/issues/17815 */
    right:0;
    width:0; /* https://projects.gwdg.de/issues/17815 */
}
.addWrapper .add.above{
    top:-0.4cm;
}
.addWrapper .add.below {
    bottom:-0.5cm;
}
.smallspace {
    margin-left: 3px;
}

.below.threequatercircle {
    border-radius: 100px;
    border-style: none solid solid;
    border-width: medium 1px 1px;
    position: relative !important;
    bottom:-0.4cm !important;
    height: 150% !important;
}
.above.threequatercircle {
    border-radius: 100px;
    border-style: none solid solid;
    border-width: medium 1px 1px;
    position: relative !important;
}
.above + .teicaret.slash { top: 0px; }

.addWrapper > svg.caretV {
    position: absolute;
    top: 0;
}

.fwWrapper {
    display: inline;
    float: right;
    color: darkslategrey !important;
    font-family: 'Lucida Console',Monaco,monospace !important;
    font-size: 9pt !important;
}

.fw {
    line-height: 120%;
    position: absolute;
    right: 0;
}
.fw.fw2 {
  top: 30px;
}

.href {
    display:none;
    font-size: 0.9em;
    letter-spacing: 0;
    position: absolute;
    width: 0;
}

.text-decoration.line-through.line-through-medium_brown_ink {
    color: inherit;
    text-decoration-color: sienna;
    -webkit-text-decoration-color: sienna;
}

.underline {
    border-bottom: 1px solid;
}
.underline .underline, .underline.double {
    border-bottom: 3px double;
}
.underline.wavy {
    border-image-repeat: repeat repeat;
    border-image-slice: 12 12 12 12;
    border-image-source: url('/public/img/border.png');
    border-left: 0 none;
    border-right: 0 none;
    border-style: none none solid;
    border-top: 0 none;
    border-bottom-width:3px;
}
.underline.underline-style.wavy.brown_ink {
    border-image-source: url('/public/img/border-brown_ink.png');
}
.underline.slightly_wavy {
    border-image-repeat: repeat repeat;
    border-image-slice: 12 12 12 12;
    border-image-source: url('/public/img/border-slightly_wavy.png');
    border-left: 0 none;
    border-right: 0 none;
    border-style: none none solid;
    border-top: 0 none;
    border-bottom-width:3px;
}
.underline.slightly_wavy.brown_ink{
    border-image-source: url('/public/img/border-slightly_wavy-brown_ink.png');
}
.underline.semicircle {
    border-bottom-left-radius: 50%;
    border-bottom-right-radius: 50%;
}
.underline.underline-style.s-shaped {
  border-image-slice: 0 0 15 0;
  border-image-source: url('/public/img/border-sshaped.png');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}
.underline.underline-style.s-shaped.brown_ink {
  border-image-source: url(/public/img/border-sshaped-brown_ink.png);
}
.underline.underline-style.s-shaped2 {
  border-image-slice: 0 0 80 0;
  border-image-source: url('/public/img/underline-style:s-shaped2.svg');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}

.underline.underline-style.half-s-shapedlower-left {
  border-image-slice: 0 0 15 0;
  border-image-source: url('/public/img/border-halfsshaped-lowerleft.png');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}
.underline.underline-style.half-s-shapedupper-left {
  border-image-slice: 0 0 15 0;
  border-image-source: url('/public/img/border-halfsshaped-upperleft.png');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}
.underline.underline-style.half-s-shapedlower-right {
  border-image-slice: 0 0 15 0;
  border-image-source: url('/public/img/border-halfsshaped-lowerright.png');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}
.underline-style.half-s-shapedlower-right.brown_ink {
  border-image-source: url('/public/img/border-halfsshaped-lowerright-brown_ink.png')
}
.underline.underline-style.half-s-shapedupper-right {
  border-image-slice: 0 0 15 0;
  border-image-source: url('/public/img/border-halfsshaped-upperright.png');
  border-left: 0 none;
  border-right: 0 none;
  border-style: none none solid;
  border-top: 0 none;
  border-width: 0 0 10px;
}
.underline-style.double-s {
    border-bottom: 10px solid;
    border-image-outset: 0 30px 0 30px;
    border-image-slice: 0 25 25 25;
    border-image-source: url('/public/img/underline-style:double-s.svg');
    border-image-width: 10px 25px 25px 20px;
}
.underline-style.double-s2 {
    border-bottom: 10px solid;
    border-image-outset: 0 30px 0 30px;
    border-image-slice: 0 0 1 0;
    border-image-source: url('/public/img/underline-style:double-s2.svg');
    border-image-width: 10px 20px 25px 1px;
}
.underline-style.d3_4-ellipse_right {
    border-image-outset: 0 20px 2px 10px;
    border-image-slice: 0 0 15 0;
    border-image-source: url('/public/img/underline-style:3_4-ellipse_right.svg');
    border-image-width: auto auto auto auto;
}
.underline-style.d3_4-ellipse_bottom {
    text-decoration: none;
    border-image-slice: 0 0 30 0;
    border-image-width: 30px 0px 20px 0px;
    border-image-outset: 0px 0px 5px 0px;
    border-image-repeat: stretch stretch;
    border-image-source: url('https://fontane-nb.dariah.eu/public/img/underline-style:3_4-ellipse_bottom.svg');
}
.underline-style.d3_4-ellipse_top {
    text-decoration: none;
    border-image-slice: 0 0 60 0;
    border-image-width: 30px 0px 20px 0px;
    border-image-outset: 0px 0px 5px 0px;
    border-image-repeat: stretch stretch;
    border-image-source: url('https://fontane-nb.dariah.eu/public/img/underline-style:3_4-ellipse_top.svg');
}
.underline.underline-style.quadruple {
    border-bottom-style: solid;
    border-bottom-width: 10px;
    border-image-slice: 0 0 30 0;
    border-image-source: url('/public/img/underline-style:quadruple.svg');
}
.underline.underline-style.triple {
    border-bottom-style: solid;
    border-bottom-width: 8px;
    border-image-slice: 0 0 30 0;
    border-image-source: url('/public/img/underline-style:triple.svg');
}
.underline.underline-style.zigzag {
    border-image: url('https://fontane-nb.dariah.eu/public/img/underline-style:zigzag.svg') 0 0 40 0 / 0 0px 10px 0px / 0px 0px 6px 0px;
}

.underline.underline-style.half-s-shapedupper-left > .underline.underline-style.half-s-shapedupper-left {
    border-image-outset: 5px 0px;
}

.underline.underline-style.bracket_top {
    border-image-slice: 0 100 100 100;
    border-image-width: 10px;
    border-image-outset: 7;
    border-image-repeat: stretch;
    border-image-source: url('/public/img/underline-style:bracket_top.svg');
}

.underline-medium_blue_pencil {
    border-color: #42b4e6;
}
.underline.underline-medium_blue_pencil {
    text-decoration-color: #42b4e6;
}
.modSeq1.text-decoration.underline.underline-medium_blue_pencil.modSeq2.pencil {
  text-decoration-color: #42b4e6;
  border-color: darkslategrey;
}
.underline .brown_ink {
    border-color: sienna;
    border-bottom-width: 1px;
    border-bottom-style: solid;
}

.fw-fontane .semicircle {
    padding: 0 8px;
}
.line-through {
    text-decoration: line-through;
}
.noLineThrough {
    text-decoration: none !important;
}
.line-through-style.triple.black_ink {
    background:
      linear-gradient(
        transparent,
        transparent calc(35% - 1px),
        darkslategrey,
        transparent 35%,
        transparent calc(55% - 1px),
        darkslategrey,
        transparent 55%,
        transparent calc(75% - 1px),
        darkslategrey,
        transparent 75%);
    text-decoration: none;
}
.line-through.retrace {
    background-image: url('/public/img/line-through-style:retrace.svg');
    background-size: 116% 95%;
}
.line-through.jagged {
    background-image: url('/public/img/jagged.png');
    background-repeat: repeat-x;
    background-size: 100% 100%;
    text-decoration: none;
}
/* .line-through.zigzag::before {
    background-image: url('/public/img/lts-jagged.svg');
    background-repeat: repeat;
    background-size: 100% 100%;
    content: '';
    height: 100%;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    transform: rotate(90deg);
    width: 100%;
} */
.line-through.zigzag::before {
    background-image: url('/public/img/lts-zigzag.svg');
    background-repeat: repeat;
    background-size: 100% 50%;
    content: ' ';
    height: 100%;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: 100%;
}
.line-through.zigzag {
    text-decoration: none;
}

.line-through.double {
    background-image: url('/public/img/ltsDouble-bg.png');
    background-repeat: repeat-x;
    background-size: 100% 100%;
    text-decoration: line-through;
}
.line-through-style.wavy {
    text-decoration: line-through wavy;
}
.text-decoration.line-through.line-through-style.half-bowpos-left {
    text-decoration: none;
    background-image: url('/public/img/line-through-style:half-bow-pos-left.svg');
    background-repeat: no-repeat;
    background-size: 100% 75%;
}
.text-decoration.line-through.line-through-style.half-bowpos-left2 {
    text-decoration: none;
    background-image: url('/public/img/line-through-style:half-bow-pos-left2.svg');
    background-repeat: no-repeat;
    background-size: 100% 75%;
}
.line-through.single_oblique90deg::before {
    border-color: inherit;
    border-top-style: solid;
    border-top-width: 2px;
    content: '';
    left: 0;
    position: absolute;
    right: 0;
    top: 50%;
    transform: rotate(90deg);
}
.line-through.single_oblique90deg {
    text-decoration:none;
}
.letter-spacing .line-through.single_oblique90deg::before {
    left: -7px;
}
.triple_oblique1 {
    border-top-color: inherit;
    border-top-style: solid;
    border-top-width: 1px;
    content: '';
    left: 0;
    position: absolute;
    right: 8px;
    top: 0;
    transform: rotate(45deg);
}
.line-through.triple_oblique45deg {
      background:
        linear-gradient(45deg,
          transparent,
          transparent calc(35% - 2px),
          black,
          transparent 35%,
          transparent calc(55% - 2px),
          black,
          transparent 55%,
          transparent calc(75% - 2px),
          black,
          transparent 75%);
      text-decoration: none;
}

.line-through-style.double_oblique45deg::before {
    border-top-color: inherit;
    border-top-style: solid;
    border-top-width: 1px;
    content: ' ';
    position: absolute;
    right: -50%;
    top: 50%;
    transform: rotate(45deg);
    width: 100%;
}

.line-through-style.double_oblique45deg::after {
    border-top-color: inherit;
    border-top-style: solid;
    border-top-width: 1px;
    bottom: 50%;
    content: ' ';
    left: 0;
    position: absolute;
    transform: rotate(45deg);
    width: 100%;
}
.line-through-style.double_oblique45deg{
    text-decoration: none;
}

.line-through-style.x.line-through-medium_blue_pencil {
    background-image: url('/public/img/line-through-style:x-blue_pencil.svg');
    background-repeat: no-repeat;
}
.line-through.single_oblique45deg {
    text-decoration:none;
}
.line-through-style.single_oblique45deg {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(47,79,79,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px));
}


.line-through.single_oblique135deg {
    text-decoration:none;
}
.line-through-style.single_oblique135deg::after {
    border-top-color: inherit;
    border-top-style: solid;
    border-top-width: 1px;
    content: ' ';
    left: -25%;
    position: absolute;
    top: 9px;
    transform: rotate(135deg);
    width: 150%;
}

.line-through-style.x {
    text-decoration: none;
}
.line-through-style.x, .line-through-style.x.line-through-medium_pencil {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(47,79,79,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px)),
        linear-gradient(135deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(47,79,79,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px));
}

.line-through-style.x.line-through-medium_blue_pencil {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(66,180,230,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px)),
        linear-gradient(135deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(50% - 1px),
            rgba(66,180,230,1) 50%,
            rgba(0,0,0,0) calc(50% + 1px));
}

.line-through.line-through-style.angle{
    text-decoration: none;
}
.line-through.angle:before, .line-through.angle:after {
    content: ' ';
    position: absolute;
    height: 100%;
}
.line-through.angle:before {
    border-left: 1px solid black;
    transform: rotate(40deg);
    top: 0px;
    position: absolute;
    left: 35%;
}
.line-through.angle:after {
    border-left: 1px solid black;
    transform: rotate(-40deg);
    top: 0px;
    position: absolute;
    right: 30%;
}
.line-through.line-through-style.double_oblique135deg {
    text-decoration: none;
}
.line-through.multiple_oblique135deg {
    text-decoration: none;
}
.line-through.multiple_oblique45deg {
    text-decoration: none;
}
.multiple_oblique45deg {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 20%,
            rgba(0,0,0,1) 20%,
            rgba(0,0,0,0) 21%,
            rgba(0,0,0,0) 39%,
            rgba(0,0,0,0) 40%,
            rgba(0,0,0,1) 40%,
            rgba(0,0,0,0) 41%,
            rgba(0,0,0,0) 60%,
            rgba(0,0,0,1) 60%,
            rgba(0,0,0,0) 61%,
            rgba(0,0,0,0) 80%,
            rgba(0,0,0,1) 80%,
            rgba(0,0,0,0) 81%);
}
.multiple_oblique135deg {
    background:
        linear-gradient(135deg,
            rgba(0,0,0,0) 20%,
            rgba(0,0,0,1) 20%,
            rgba(0,0,0,0) 21%,
            rgba(0,0,0,0) 39%,
            rgba(0,0,0,0) 40%,
            rgba(0,0,0,1) 40%,
            rgba(0,0,0,0) 41%,
            rgba(0,0,0,0) 60%,
            rgba(0,0,0,1) 60%,
            rgba(0,0,0,0) 61%,
            rgba(0,0,0,0) 80%,
            rgba(0,0,0,1) 80%,
            rgba(0,0,0,0) 81%);
}
.multiple_oblique135deg.multiple_oblique45deg {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(20% - 1px),
            rgba(0,0,0,1) 20%,
            rgba(0,0,0,0) calc(20% + 1px),
            rgba(0,0,0,0) calc(40% - 1px),
            rgba(0,0,0,1) 40%,
            rgba(0,0,0,0) calc(40% + 1px),
            rgba(0,0,0,0) calc(60% - 1px),
            rgba(0,0,0,1) 60%,
            rgba(0,0,0,0) calc(60% + 1px),
            rgba(0,0,0,0) calc(80% - 1px),
            rgba(0,0,0,1) 80%,
            rgba(0,0,0,0) calc(80% + 1px)),
        linear-gradient(135deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(20% - 1px),
            rgba(0,0,0,1) 20%,
            rgba(0,0,0,0) calc(20% + 1px),
            rgba(0,0,0,0) calc(40% - 1px),
            rgba(0,0,0,1) 40%,
            rgba(0,0,0,0) calc(40% + 1px),
            rgba(0,0,0,0) calc(60% - 1px),
            rgba(0,0,0,1) 60%,
            rgba(0,0,0,0) calc(60% + 1px),
            rgba(0,0,0,0) calc(80% - 1px),
            rgba(0,0,0,1) 80%,
            rgba(0,0,0,0) calc(80% + 1px));
}
.multiple_oblique135deg.multiple_oblique45deg.brown_ink {
    background:
        linear-gradient(45deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(20% - 1px),
            rgba(160,82,45,1) 20%,
            rgba(0,0,0,0) calc(20% + 1px),
            rgba(0,0,0,0) calc(40% - 1px),
            rgba(160,82,45,1) 40%,
            rgba(0,0,0,0) calc(40% + 1px),
            rgba(0,0,0,0) calc(60% - 1px),
            rgba(160,82,45,1) 60%,
            rgba(0,0,0,0) calc(60% + 1px),
            rgba(0,0,0,0) calc(80% - 1px),
            rgba(160,82,45,1) 80%,
            rgba(0,0,0,0) calc(80% + 1px)),
        linear-gradient(135deg,
            rgba(0,0,0,0) 0%,
            rgba(0,0,0,0) calc(20% - 1px),
            rgba(160,82,45,1) 20%,
            rgba(0,0,0,0) calc(20% + 1px),
            rgba(0,0,0,0) calc(40% - 1px),
            rgba(160,82,45,1) 40%,
            rgba(0,0,0,0) calc(40% + 1px),
            rgba(0,0,0,0) calc(60% - 1px),
            rgba(160,82,45,1) 60%,
            rgba(0,0,0,0) calc(60% + 1px),
            rgba(0,0,0,0) calc(80% - 1px),
            rgba(160,82,45,1) 80%,
            rgba(0,0,0,0) calc(80% + 1px));
}

.cancelLeft::before {
    content: '|';
}
.cancelRight::after {
    content: '|';
}

/* hash encoded within tei:del/@rend */
.del.hash::before {
  bottom: 0;
  content: '#';
  position: absolute;
  top: -8px;
  width: 100%;
}




.border-right.solid{
    border-right: 1px solid;
}
.border-left.solid{
    border-left: 1px solid;
}
.border-top.solid{
    border-top: 1px solid;
}
.border-bottom.solid{
    border-bottom: 1px solid;
}
.border-right-style.wavy{
    border-style: solid;
    border-width: 0 7px 0 0;
    -moz-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    -webkit-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    -o-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    border-image: url('/public/img/border-wavy.png') 27 27 26 fill repeat;
}

.border-left-style.wavy {
    border-left-style: solid;
    border-width: 0 0 0 7px;
    -moz-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    -webkit-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    -o-border-image: url('/public/img/border-wavy.png') 27 27 26 repeat;
    border-image: url('/public/img/border-wavy.png') 27 27 26 fill repeat;
}
.border-left-style.retrace{
    border-image-slice: 13 27 55 95;
    border-image-width: 0px 0px 0px 20px;
    border-image-outset: 0px 0px 0px 20px;
    border-image-repeat: stretch stretch;
    border-image-source: url('/public/img/border:retrace.svg');
}
.border-left-style.retrace.border-medium_red_pencil{
        border-image-source: url('/public/img/border:retrace-red_pencil.svg');
}
.border-style.retrace {
    border-image-slice: 64 72 60 84;
    border-image-outset: 0px 0px 0px 0px;
    border-image-repeat: stretch stretch;
    border-image-source: url('/public/img/border:retrace-all.svg');
}

.border-top.solid.border-left.solid.border-style.retrace {
    border-image-width: 20px 0 0 20px;
}

.border-radius50{
    border-radius:50%;
}

.border-left.double {
    border-left: 4px double;
}
.border-left.double.border-left-style.wavy {
    border-left: 7px solid;
}
.border-left.triple {
    border-image-slice: 1 1 1 10;
    border-image-width: 0px 0px 0px 10px;
    border-image-outset: 0px 0px 0px 5px;
    border-image-repeat: stretch stretch;
    border-image-source: url('/public/img/border:triple.svg');
}

.border-left.double .border-left.double {
    border-image-slice: 1 1 1 14;
    border-image-width: 0px 0px 0px 10px;
    border-image-outset: 0px 0px 0px 7px;
    border-image-repeat: stretch stretch;
    border-image-source: url('/public/img/border:quad.svg');

}

.border-left-style.brace {
/*  border-image-slice: 0 0 0 10;  */
/*  border-image-source: url('/public/img/bracket-left.svg');  */
/*  border-image-width: 0 0 0px 10px;  */
    border-left:0;
}

.border-medium_blue_pencil {
    border-color: #42b4e6 !important;
}
.border-medium_red_pencil {
    border-color: red !important;
}
.border.solid.border-radius50.border-medium_red_pencil {
    border: 1px solid red;
}
.border.solid {
    border: 1px solid;
}
.border-top.dashed {
    border-top-style: dashed !important;
}
.border-top-color.red {
    border-top-color: red !important;
}
.padding-right.d05cm {
    padding-right: 0.5cm;
}
.border-right.dashed {
    border-right-width: 1px;
    border-right-style: dashed;
}
.border-bottom.solid.border-bottom-style.brace {
    border-image-slice: 38 0 38 0;
    border-image-width: 0 0 1cm 0;
    border-image-outset: 0 0 38px 0;
    border-image-source: url('/public/img/horizontale-geschweifte-klammer.svg');
}

.metamark.integrate.bracket_bottom {
    border-image-slice: 38 0 38 0;
    border-image-width: 0 0 1cm 0;
    border-image-outset: 0 0 0 0;
    border-image-source: url('/public/img/horizontale-geschweifte-klammer.svg');
    border-bottom: 1cm solid transparent;
    display: block;
}

.fraction {
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    line-height: 100%;
}
.fraction .bottom {
    border-top: 1px solid;
    display: block;
}
/*
.g.mgem:before {content:'\E0000'}
.g.ngem:before {content:'\E0001'}
*/
.g.rth {
    display: inline-block;
    top: 5px;
    width: 10px;
}

.instant:after {content:'\21AF'}
.unclear:before , .unclear:after{
    content: '?';
  font-size: 0.8em;
  vertical-align: super;
  line-height: 100%;
}

.instantHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -1.9cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	transform-origin: center center 0;
}
.del.instant:hover > .instantHover {display:block}
.del.instant {display:inline-block;}

.restore {
    text-decoration: underline dotted;
}
.restore:hover .restoreHover {
    display: block;
}

.gap.illegible.mm4:after {
  content: 'xxxxxx';
  max-width: 4mm;
  display: inline-block;
 /* position: absolute; */
  overflow:hidden;
  height:100%;
  font-style: italic;
}

.gap.illegible.mm19::after {
    content: 'xxxxxxxx';
    display: table-cell;
    font-style: italic;
    max-width: 19mm;
    overflow: hidden;
}
/* characters */

    /* no case */

.illegible.chars1::after {
    content: 'x';
    font-style: italic;
}

    /* lower case */

.illegible.lc_chars1::after {
    content: 'x';
    font-style: italic;
}
.illegible.lc_chars2::after {
    content: 'xx';
    font-style: italic;
}
.illegible.lc_chars3::after {
    content: 'xxx';
    font-style: italic;
}

    /* upper case */

.illegible.uc_chars1::after {
    content: 'X';
    font-style: italic;
}
.illegible.uc_chars2::after {
    content: 'XX';
    font-style: italic;
}
.illegible.uc_chars3::after {
    content: 'XXX';
    font-style: italic;
}

/* words */
    /* capital */
.illegible.cap_words1::after {
    content: 'X---x';
    font-style: italic;
}
.illegible.cap_words2::after {
    content: 'X---x X---x';
    font-style: italic;
}
    /* uncap */
.illegible.uncap_words1::after {
    content: 'x---x';
    font-style: italic;
}
.illegible.uncap_words2::after {
    content: 'x---x x---x';
    font-style: italic;
}
.illegible.uncap_word_chars5:after{
  content: 'x---x';
  font-style: italic;
}

.letter-spacing {
    margin-left: 5px;
    margin-right: 5px;
}

/* Absatzlinie */
hr.Absatzlinie {
    border-color: inherit;
    color: black;
    margin: 0;
    padding: 10px;
    position: relative;
}
hr.Absatzlinie + hr.Absatzlinie {
    margin-left: 8px;
    margin-top: 1px;
}

/* Abgrenzungslinie */
hr.Abgrenzungslinie {
    border-color: inherit;
    color: black;
    margin: 0;
    position: relative;
}
hr.Abgrenzungslinie + hr.Abgrenzungslinie {
    margin-left: 8px;
    margin-top: 1px;
}
hr.Abgrenzungslinie.Halbkreis {
    margin: 0;
    position: relative;
    border: solid 1px;
    height: 1cm;
    border-radius: 50%;
    border-bottom: none;
    border-right: none;
    border-left: none;
}

/* Schlusslinie */
hr.Schlusslinie {
    border-color: inherit;
    color: black;
    margin: 0;
    position: relative;
}
.hrHover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	font-style: italic;
	transform-origin: center center 0;
}
hr:hover + .hrHover {
    display: block;
}
.zone.figure:hover > .hrHover {
    display: block;
}
svg:hover + .hrHover {
    display: block;
}
.hrHover:hover {
    display: block;
}
/* damage */
.damage.mm::after {
    content: 'x---x x---x';
    font-style: italic;
}

/* Sonderzeichen */
.sonderzeichen > .hover, .g.rth > .hover {
    display: none;
    background-color: rgba(100, 100, 100, 0.75);
    font-size: 0.9em;
    color: white;
    white-space: nowrap;
	position: absolute;
	margin-top: -0.5cm;
	padding: 0px 10px;
	z-index: 100;
	line-height: 1cm;
	font-size: 0.4cm;
	font-style: italic;
	transform-origin: center center 0;
}

.sonderzeichen:hover > .hover, .g.rth:hover > .hover {
    display:inline-block;
}

/* rendition */
.Kalenderblatt .aligncenter{
    text-align:center;
}
.line.flexjustify {
    display: flex;
    justify-content: space-between;
}
/* milestone */
span.milestone.paragraph:after {
  content: 'milestone: Absatz';
  position: absolute;
  right: -130px;
  border: 1px solid black;
  border-top-right-radius: 21px;
  padding: 3px;
  border-bottom-left-radius: 21px;
  font-family: monospace;
  font-size: 9pt;
  background-color: lightgray;
}

span.milestone.paragraph {
  border-bottom: 1px solid black;
  display: block;
}

/* space */
.space.cm2 {
    width: 2cm;
    display: inline-block;
}


#newfremde_Hand1:before,
#newfremde_Hand3:before,
#newunbekannte_p:before,
#newFriedrich_Fontane:before,
#newDruck_p:before,
#newStempel2:before,
#newDruck_z:before,
#newKersten:before,
#newStempel5:before,
#newStempel_3:before,
#newfremde_Hand2:before,
#newStempel1:before,
#newEmilie_Fontane-Schreiberin:before,
#newunbekannt_p:before,
#newfremde_Hand6:before,
#newfremde_Hand5:before,
#newfremde_Hand11:before,
#newStempel_2:before,
#newEmiie_Fontane-Schreiberin:before,
#newfremde_Hand_2:before,
#newStempel9:before,
#newunbekannt_z:before,
#newunbekannte_z:before,
#newStempel10:before,
#newStempel11:before,
#newunbekannte_z-unsicher:before,
#newunbekannt_z-unsicher:before,
#newfremde_Hand4:before,
#newArchviar2:before,
#newStempel8:before,
#newHübner:before,
#newLucae:before,
#newfremde_Hand9:before,
#newEduard_Ponge:before,
#newFontnae:before,
#newfremde Hand6:before,
#newFriedrich_Fontane:before {
    content: '✍';
    position: absolute;
    margin-left: -20px;
}

"
(: debugging features
"
.surface {border: 2px solid red;}
.zone {border: 2px solid lightpink;}":)
,
(: special treatment :)
"

#A06 #sBeilage_Ir {
    margin-top: 6cm;
    transform: rotate(270deg);
    z-index: 100;
    background: whitesmoke;
}

#A06 #s13v div.zone.illustration.figure {
    z-index: -1;
}

#A07 .sourceDoc #s27r.surface div.zone div.line span.mod.subst span.addWrapper {
    position: relative;
    top: -0.65cm;
}
#A09 #s55r.surface div.zone span.metamark.deletion.pencil {
    background: linear-gradient(65deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) calc(50% - 1px), rgba(47,79,79,1) 50%, rgba(0,0,0,0) calc(50% + 1px));
    margin-left: -133px;
}
#A19_12v_1 {
    margin-left: 0 !important;
}
#s7r > div:nth-child(4) > div:nth-child(3) > div > div:nth-child(1) {
    margin-left: 2cm !important;
}
#s7r > div:nth-child(4) > div:nth-child(3) > div > div:nth-child(2) {
    margin-left: 2.5cm !important;
}
#B02 #s30r.surface div.zone.border-left.solid.border-left-style.brace.black_ink svg {
    margin-left: -20px;
}
#B02.sourceDoc #s77r.surface div.zone {
    line-height: 0 !important;
}
#B09_58r_1.zone span span.addWrapper svg {
    top: 0;
}
#s60v > div:nth-child(1) > div.line.margin-left.d30cm > span > span > span:nth-child(3) > span.addWrapper > svg {
    top: -14px !important;
}
#B10 #s12v > div > div.line.margin-left.d12cm > span.mod.subst {
    width: 9%;
}

#B11 #s22r.surface div.zone div.line {
    line-height: 0;
}
#B06 #s18r.surface div.zone span.metamark.authorial_note {
    display: block;
    transform: scaleX(0.2);
}

#s1r > div:nth-child(4) > div.line.margin-left.d22cm > span:nth-child(3) > span.addWrapper > svg {
    top: -8px !important;
}
#s12r > div.zone.text-decoration.line-through.line-through-style.single_oblique45deg::after {
    left: 14px;
    top: 27px;
    width: 50%;
}
#s14r > div:nth-child(1) > div:nth-child(8) {
    margin-top: 6px;
}
#s16r > div > div:nth-child(16) > span:nth-child(3) > span.addWrapper > span {
    left: -1.2cm !important;
}
#s21r > div > div > span.mod.subst > span.addWrapper{
    position: relative;
}
#s22r > div:nth-child(2) > div:nth-child(6) > span:nth-child(5) > span.addWrapper > svg {
    right: 0;
}
#B15 #s4r.surface > div:nth-child(6) {
    line-height: 0.45cm !important;
}
#B15 #s55v > div > div.line.margin-left.d5cm > span:nth-child(2) > span.addWrapper > .add.above {
    left: -0.7cm !important;
}
#B15 #s55v > div > div.line.margin-left.d5cm > span:nth-child(2) > span.addWrapper {
    left: 0.1cm;
}
#B13 #s32v > div:nth-child(3) > div:nth-child(4) > span.mod.subst > span.addWrapper {position: relative;}
#B13 #s32v > div:nth-child(3) > div:nth-child(4) > span.mod.subst > span.addWrapper > span {left: -1.2cm !important;}

#B14_59v_10 > div > div.line.margin-left.d5cm > span.anchored > span > span.addWrapper > svg { right:-82px !important; }

#B02 #s77r > div:nth-child(2) { line-height:inherit !important; }
#B02 #B02_10r_3 > span > span { left:12px; }
#B10_31r_1 > .zone + .zone {
    line-height: 0.45cm !important;
}
#B10_14v_1 > .zone {
    line-height: 1cm !important;
}
#B10 #B10_47r_1.zone div.zone.border-top.solid.border-left.solid.border-style.retrace div.zone {
    position:relative !important;
}

#B10_28v_1 + div.zone {
    top: 12.8cm !important;
}
#B11 #s14r > div:nth-child(2) > div.zone.text-decoration.line-through.line-through-style.single_oblique45deg.line-through-medium_brown_ink::after {
    border-left-color: inherit;
    border-left-style: solid;
    border-left-width: 2px;
    content: ' ';
    right: 50%;
    position: absolute;
    bottom: 44%;
    transform: rotate(-45deg);
    transform-origin: center center;
    height: 25%;
    left: initial;
    top: inherit;
    width: initial;
}
#B05 #s5r div.zone:nth-child(2) div.zone.verse:nth-child(1) div.line:nth-child(1) span.seg:nth-child(1) span.anchored span span.addWrapper {
    margin-top: -0.2cm;
    position: absolute;
}
#B05 #s7r > div:nth-child(2) > div:nth-child(5) > div.line.margin-left.d4cm > span:nth-child(1) > span.addWrapper {left:37px !important;}
#B05 #s32v > div > div {
    top: inherit !important;
    margin-top: -1.6cm;
    margin-left: 1.3cm;
}

#B06 #s18r span.metamark span.bracket_right {
  transform: scaleX(0.63) !important;
  margin-left: -20px;
}

#B06_24r_1.zone span.modSeq1.text-decoration.underline {
    border-bottom: none !important;
}
#B06 #s38r.surface div.zone.highlighted svg#svg3383 {
    margin-left: -10px;
}
#B07 #B07_47v_2 > div:nth-child(4) > span:nth-child(2) > span.addWrapper > span {
    left: -0.5cm !important;
}
#B07 #s47 #B07_47v_2 div.line span span.addWrapper svg.curved_V {
  top: -13px !important;
}
#B07 #B07_47v_2 > svg {
    left: -25px;
    padding: 5px;
}
#B07_49r_1 {
  z-index: -1;
}
#B07 #s49v > div:nth-child(3) > span > span {
    position: absolute;
    top: 40px;
}
#B11 #s14r > div:nth-child(2) > div.zone.text-decoration.line-through.line-through-style.single_oblique135deg.line-through-medium_brown_ink::after {
    left: inherit;
    position: absolute;
    width: 20%;
    right: 40%;
    bottom: 92%;
}
#B11 #s1r > div:nth-child(1) {z-index:100;}

#B11 #s24r.surface div#B11_24r.zone span.metamark.authorial_note.pencil {
    left: -150%;
}
#B11 #s25r.surface div.zone span.metamark.authorial_note.pencil{
    bottom: 60% !important;
    left: -100%;
}
#B11 #s26r .metamark.integrate {
    bottom: 50%;
    position: absolute;
    left: -130%;
}

#B11 #B11_49v_2.zone > div.zone.highlighted.border-left.double{
    border:none !important;
}
#B14_59v_10 svg.upper-right {
    margin-right: -59px;
}

#B14 #s44r.surface div.zone:nth-child(4) {
    width: 0 !important;
}
#C05_3rar_1.zone div.line span span.addWrapper svg.curved_V {
    top:-12px !important;
}
#C05_18r_3 {
    left: 3.8cm !important;
}
#C06 #s16r .metamark .bracket_left {right:inherit;}

#D08 #s68r.surface div.zone.illustration.figure div.zone:nth-child(14) div.line:nth-child(3) {
  font-size: 0.85em;
}
#D08 #s60v.surface div.zone.illustration.figure {
  top: 7.4cm !important;
}
#D08 #s37v.surface div.zone.illustration.figure div.zone{
  top: 1.2cm;
  left: 5.6cm;
}
#D08 #s30r.surface div.zone div.line span span.addWrapper svg.caretV {
  margin-left: -0.7cm !important; top: -3px;
}
#D08 #s9r.surface div.zone:nth-child(4) {
  left: initial !important;
}
"
(: bugfixes :),
"
*:hover > div {letter-spacing: initial;}
",
if (contains(request:get-cookie-value('facs'), 'inactive')) then '.facs {display:none;}' else (),
if (contains(request:get-cookie-value('trans'), 'inactive')) then '.surface {display:none;}' else (),
if (contains(request:get-cookie-value('xml'), 'inactive')) then '.teixml {display:none;}' else '.teixml {display:block;}'
