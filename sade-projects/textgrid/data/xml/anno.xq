xquery version "3.1";

import module namespace console="http://exist-db.org/xquery/console";
import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "/db/apps/SADE/modules/fontane/transform.xqm";
import module namespace xqjson="http://xqilla.sourceforge.net/lib/xqjson";

declare default element namespace "http://www.tei-c.org/ns/1.0";
(::)
(:declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";:)
(:declare namespace json="http://www.json.org";:)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace digilib="digilib:digilib";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace xlink="http://www.w3.org/1999/xlink";

declare option exist:serialize "method=text media-type=application/json omit-xml-declaration=yes";


(: see: http://exist-db.org/exist/apps/doc/xquery.xml#serialization :)
(:declare option output:method "json";:)
(:declare option output:media-type "application/json";:)

(:declare option output:indent "yes";:)
(:declare option exist:serialize "indent=yes";:)

declare variable $tei := request:get-parameter("tei", "textgrid:2smvd.0");
declare variable $img := request:get-parameter("img", "textgrid:164h2.1");
declare variable $surfaces := collection('/db/sade-projects/textgrid/data/xml/data/')//tei:sourceDoc/tei:surface[ends-with(@facs, substring-before($img, "."))];

declare variable $xywh :=
map:new(
    for $surface in $surfaces
    let $iiifUrlArr := fontaneTransfo:digilib($surface) =>  tokenize('/')
    let $pctArr := $iiifUrlArr[starts-with(., 'pct:')] => substring(5) => tokenize(',')
    let $wx := $pctArr[1] cast as xs:float
    let $wy := $pctArr[2] cast as xs:float
    let $wh := $pctArr[4] cast as xs:float
    let $ww := $pctArr[3] cast as xs:float

    let $ix := doc("/db/sade-projects/textgrid/data/xml/data/217qs.xml")//digilib:image[@uri=substring-before($img, ".")]

    let $ixw := $ix/@width cast as xs:integer
    let $ixh := $ix/@height cast as xs:integer

    let
        $x := ($wx div 100) * $ixw,
        $y := ($wy div 100) * $ixh,
        $w := ($ww div 100) * $ixw,
        $h := ($wh div 100) * $ixh
    return
        map:entry(string($surface/@n), ($x, $y, $w, $h))
);

declare variable $dpcm := 236.2205;

declare function local:dispatch($node as node()) as item()* {
let $sf := string($node/ancestor-or-self::tei:surface[parent::tei:sourceDoc]/@n)
let
    $x := $xywh($sf)[1],
    $y := $xywh($sf)[2],
    $w := $xywh($sf)[3],
    $h := $xywh($sf)[4]

return
typeswitch($node)

(:    case element(tei:zone):)
(:        return <div>{local:passthru($node)}</div>:)

    case element(tei:zone)
        return
            let $style := data($node/@style)
            let $bla := console:log($style)

            let $ulx := if($node/@ulx) then number($node/@ulx) else 0
            let $zx := $x + floor( ( $ulx * $dpcm  )  )

            let $uly := if($node/@uly) then number($node/@uly) else 0
            let $zy := $y + floor( ( $uly * $dpcm  )  )

            let $zw := if( $node/@lrx ) then $w - $zx - number($node/@lrx) * $dpcm else $zx - $w
            let $zh := if( $node/@lry ) then $h - $zy - number($node/@lry) * $dpcm else $h - $zy

            return


                <item type="object">
                    <pair name="@id" type="string">_:N{util:uuid()}</pair>
                    <pair name="@type" type="string">oa:Annotation</pair>
                    <pair name="motivation" type="string">sc:painting</pair>
                    <pair name="resource" type="object">
                        <pair name="@id" type="string">_:N{util:uuid()}</pair>
                        <pair name="@type" type="string">cnt:ContentAsText</pair>
                        <pair name="format" type="string">text/plain</pair>
                        <pair name="chars" type="string">{$node//text()}</pair>
                        <pair name="language" type="string">de</pair>
                    </pair>
                    <pair name="on" type="string">https://textgridlab.org/1.0/iiif/manifests/{$tei}/canvas/{$img}.json#xywh={$zx},{$zy},50,50</pair>
                </item>
    case text()
        return replace($node, '"', "&quot;")
    default
        return local:passthru($node)

};

declare function local:passthru($nodes as node()*) as item()* {
    for $node in $nodes/node() return local:dispatch($node)
};


let $imgurl := "http://textgridrep.org/"||$img


let $xml :=
<json type="object">
    <pair name="@context" type="string">http://www.shared-canvas.org/ns/context.json</pair>
    <pair name="@id" type="string">http://localhost:8080/exist/rest/apps/SADE/modules/fontane/anno.xq?tei={$tei}&amp;img={$img}</pair>
    <pair name="@type" type="string">sc:AnnotationList</pair>
    <pair name="resources" type="array">
        {$surfaces//tei:zone ! local:dispatch(.)}
        <!--         {$surfaces//tei:zone[@uly][contains(., "STAATSBIBLIOTHEK")] ! local:dispatch(.)}
 -->
    </pair>
</json>

let $egal := response:set-header("Access-Control-Allow-Origin", "*")

(:let $xml := :)
(:<json type="object">:)
(:<pair name="@context" type="string">http://www.shared-canvas.org/ns/context.json</pair>:)
(:<pair name="@id" type="string">http://localhost/iiif/ann1.json</pair>:)
(:<pair name="@type" type="string">sc:AnnotationList</pair>:)
(:<pair name="resources" type="array">:)
(:<item type="object">:)
(:<pair name="@id" type="string">_:N9ffd445be244485d9cbff9fcdc9dcbd4</pair>:)
(:<pair name="@type" type="string">oa:Annotation</pair>:)
(:<pair name="motivation" type="string">sc:painting</pair>:)
(:<pair name="resource" type="object">:)
(:<pair name="@id" type="string">_:N3dbab1b51a6646c390ab3fae21c4c3af</pair>:)
(:<pair name="@type" type="string">cnt:ContentAsText</pair>:)
(:<pair name="format" type="string">text/plain</pair>:)
(:<pair name="chars" type="string">no[n] habetis Nondu[m] cognoscitis</pair>:)
(:<pair name="language" type="string">lat</pair>:)
(:</pair>:)
(:<pair name="on" type="string">http://textgridlab.org/1.0/iiif/manifests/textgrid:2smvd.0/canvas/textgrid:164g9.1.json#xywh=1711,557,1208,224</pair>:)
(:</item>:)
(:</pair>:)
(:</json>:)

return
    xqjson:serialize-json($xml)

