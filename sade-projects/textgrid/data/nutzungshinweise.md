# Nutzungshinweise

Alle auf dieser Webseite präsentierten und im Rahmen des Projekts erarbeiteten Inhalte der Edition sind unter der Lizenz [„CC-BY-NC-ND-4.0 international“](https://creativecommons.org/licenses/by-nc-nd/4.0/) zugänglich.





Die Digitalisate der Notizbücher Theodor Fontanes stehen unter der Lizenz [„CC0“](https://creativecommons.org/publicdomain/zero/1.0/) zur Verfügung.





Die ODD wird auf [github.com](https://github.com/martin-delaiglesia/Fontane-Notizbuecher) unter LGPL 3.0 Lizenz zur Verfügung gestellt.

Die Software ist als eine Applikation für [eXist-db](http://exist-db.org) entwickelt und wird auf [gitlab.gwdg.de](https://gitlab.gwdg.de/fontane-notizbuecher/) unter LGPL 3.0 Lizenz zur Verfügung gestellt.

Für die Benutzung dieser Webseite ist der Einsatz von [Cookies](https://de.wikipedia.org/wiki/Cookie) notwendig. Dabei werden keinerlei persönliche Informationen abgefragt und gespeichert; sie dienen der Speicherung der bevorzugten Ansichten innerhalb der Edition. Keine dieser Daten werden auf dem Server gespeichert.
