jQuery(function($) {


$(document).on("scroll",function(){
    if($(document).scrollTop()>70){
        $("header").removeClass("thenormalone").addClass("thesixtyfive");$("#toc-helper").css("top", "45px");
    } else{
        $("header").removeClass("thesixtyfive").addClass("thenormalone");$("#toc-helper").css("top", "85px");
    }
});
var countclick = 0;
$('#left-toggle').click(function(e) {
     e.stopPropagation();
countclick++;
$( "#cssmenu-vertical2, #cssmenu-vertical1, #dfgmenu" ).fadeToggle("slow", function(){
	if (countclick % 2 === 0) {
		$(".full-width").addClass("main-width").removeClass("full-width");
		$("#left-toggle").removeClass("small-left");
		}
	else {
		$(".main-width").addClass("full-width").removeClass("main-width");
		$("#left-toggle").addClass("small-left");
		}
});});

$.holdReady(true);
$.getScript("http://fontane-nb.dariah.eu:8080/js/jail.js", function() {
$.holdReady(false);
});
$( ".lastmod" ).load( "http://fontane_nb.dariah.eu:8080/exist/rest/apps/sade/sandbox/lastMod2.xql" );

$(document).ready(function () {
$("#kasten-a").load("/exist-2.1-rev/rest/apps/sade/sandbox/tgbrows-alternative.xql?tgb.kasten=a");
$("#kasten-b").load("/exist-2.1-rev/rest/apps/sade/sandbox/tgbrows-alternative.xql?tgb.kasten=b");
$("#kasten-c").load("/exist-2.1-rev/rest/apps/sade/sandbox/tgbrows-alternative.xql?tgb.kasten=c");
$("#kasten-d").load("/exist-2.1-rev/rest/apps/sade/sandbox/tgbrows-alternative.xql?tgb.kasten=d");
$("#kasten-e").load("/exist-2.1-rev/rest/apps/sade/sandbox/tgbrows-alternative.xql?tgb.kasten=e");

if(window.location.href.search(/viewer.item=[a-z0-9]{5,7}\.\d{1,3}/) < 0) {
    $('#tg-lastmod').load('/exist-2.1-rev/apps/sade/sandbox/lastMod2.xql');
    }
if(window.location.href.search(/viewer.page=.+,.+,.+,.+/) > 0) {
    $('.content').css('width', '45cm');
    }
if(window.location.href.search(/trigger=.*?fullscreen/) > 0) {
    $( "#left-toggle" ).trigger( "click" );
    }
});

$(function() {
//Options hardcoded in JS!!!
$("#toc").tocify();
});
// All from XSLT
        var countTOCtoggle = 0;
        $("#TOCtoggle1, #TOCtoggle2").click(function () {
            countTOCtoggle++;
            if (countTOCtoggle % 2 === 0) {
                $('#toc').css('height', 'calc(100% - 280px)');
                $('#toc').css('height', '-webkit-calc(100% - 280px)');
            }
            else {
                $('#toc').css('height', '35px');
            }
        });
        $( "div.ruler" ).draggable();
        $( "div.ruler" ).tooltip();
        $( "div.facs" ).tooltip();
        $( "span.add_del" ).each(function(){
            var pre =  $(this).attr("data-pre");
            var del = $(this).attr("data-overwritten");
            var add = $(this).attr("data-this");
            var post = $(this).attr("data-post");
            $(this).tooltip({
            content: pre + "<span style='font-style: italic;'>&lt;</span>" + del + "<span style='font-style: italic;'> Ã¼berschrieben </span>" + add + "<span style='font-style: italic;'>&gt;</span>" + post,
            });
        });
        
        $( ".retrace").tooltip({
            content: function(){ return $(this).attr('data-prev') + "<span style='font-style: italic;'>&lt;</span>" + $(this).attr('data-this') + " <span style='font-style: italic;'>nachgezogen</span> " + $(this).attr('data-this') + "<span style='font-style: italic;'>&gt;</span>" + $(this).attr('data-following') }
        });
        //   $( ".retraced").tooltip({content: "<span style='font-style: italic;'>nachgezogen</span>"});
        $("*[style~='font-size:large']").css('font-size', '19pt');
        $("*[style~='font-size:x-large;']").css('font-size', '24pt');
        $("[style~='font-size:large']").css('font-size', '19pt');
        $("[style~='font-size:x-large;']").css('font-size', '24pt');
        $("[style~='x-large;']").css('font-size', '24pt');
        $("[style~='large;']").css('font-size', '19pt');
        
        $( "span.abbr" ).tooltip();
        $( ".ulxuly" ).tooltip();
        $( ".lrxlry" ).tooltip(); 
  		
  		$("button.font").click(function () {
        $(".surface, div.surface span").toggleClass("single-font");
        });

	    $(".figure").click(function () {
        $(".figuretext").toggleClass("hidden");
        });

        $("#buttons-button").click(function () {
        $("#buttons-helper").toggleClass("visible");
        });
        $("button.abbr").click(function () {
        $(".abbr").toggleClass("highabbr");
        $(".abbr").toggleClass("PseudoIEclass");
        });
        
        $("button.gap").click(function () {
        $(".gap").toggleClass("highgap");
        $(".gap_info").toggle();
        });

        $("button.date").click(function () {
        $(".date").toggleClass("highdate");
        $(".date").toggleClass("PseudoIEclass");
        });
        
        $("button.person").click(function () {
        $(".person").toggleClass("highperson");
        $(".person").toggleClass("PseudoIEclass");
        });
        
        $("button.direct").click(function () {
        $(".direct").toggleClass("highdirect");
        $(".direct").toggleClass("PseudoIEclass");
        });
        
        $("button.place").click(function () {
        $(".place").toggleClass("highplace");
        $("html body div.buttons div.button ul.hidden#entities").toggleClass("visible");
        $(".place").toggleClass("PseudoIEclass");
        });

        $("button.hi").click(function () {
        $(".hi").toggleClass("highhi");
        $(".hi").toggleClass("PseudoIEclass");
        });

        $("button.mod").click(function () {
        $(".mod").toggleClass("highmod");
        $(".mod").toggleClass("PseudoIEclass");
        });
        
        $("button.del").click(function () {
        $(".del").toggleClass("highdel");
        $(".del").toggleClass("PseudoIEclass");
        });
        
        $("button.xpath").click(function () {
        $("span.xpath").toggleClass("visible");
        $("span.xpath").toggleClass("PseudoIEclass");
        });

        $("button.linethrough").click(function () {
        $(".linethrough").toggleClass("highlinethrough");
        $(".linethroughblue").toggleClass("highlinethrough");
        $(".linethroughblue", ".linethrough").toggleClass("PseudoIEclass");
        });

        $("button.linenumberbutton").click(function () {
        $(".hidelinenumber").toggleClass("linenumber");
        $(".hidelinenumber").toggleClass("PseudoIEclass");
        });
        $("button.viewFacs").click(function () {
      //  $("div.ruler").toggleClass("visible"); //
        $(".surface").toggleClass("surfacefacs");
        $(".facs").toggleClass("visible");
        $(".thumbs").css("margin-left", "24cm");
        var y = $(window).scrollTop();  //current y position
        $(window).scrollTop(y-10);
        $(window).scrollTop(y+10);
                $(function(){
                        pageurl = location.href;
                        if('.facs:visible') {
                            appendix = '&facs=1';
                        }
                        else {
                            appendix = '&facs=0';
                        }
                        window.history.pushState({path:pageurl},'',location.pathname.concat(location.search, appendix));
                    });
        });
        
        $("button.ruler-button").click(function () {
        $("div.ruler").toggleClass("visible");
        $("div.ruler").toggleClass("PseudoIEclass");
        });
        
        $("button.ink").click(function () {
        $(".brown_ink").toggleClass("high_brown_ink");
        $(".black_ink").toggleClass("high_black_ink");
        $(".ink").toggleClass("high_ink");
        $("ul.hidden#medium").toggleClass("visible");                    
        });
        
        $("button.characteristics").click(function () {
        $(".clean").toggleClass("high_clean");
        $(".hasty").toggleClass("high_hasty");
        $("#characteristics").toggleClass("visible");                    
        });
        
        $("button.scriptbutton").click(function () {
        $(".Latn").toggleClass("high");
        $("button.scriptbutton").toggleClass("high");
        $(".Latn").toggleClass("PseudoIEclass");
        });
        
		$('img.feder_Friedrich_Fontane').mouseover(function() {
		$('.Friedrich_Fontane').toggleClass("highFriedrich_Fontane");
		});
		
		$('img.feder_fremde_Hand1').mouseover(function() {
		$('.fremde_Hand1').toggleClass("highfremde_Hand1");
		});
		
		$('img.feder_fremde_Hand2').mouseover(function() {
		$('.fremde_Hand2').toggleClass("highfremde_Hand2");
		});
		
		$('img.feder_fremde_Hand3').mouseover(function() {
		$('.fremde_Hand3').toggleClass("highfremde_Hand3");
		});

        $("div.zone").click(function () {
        $(this).fadeTo("slow", 1);
        });                   
        if(window.location.href.search(/trigger=.*?facs/) > 0) {
            $( "#viewfacs" ).trigger( "click" );
            }
		$(function(){
			$('img.lazy').jail({
			    offset: 1300,
			    timeout: 0,
				// event: 'click', 
			   placeholder : 'http://fontane-nb.dariah.eu:8080/js/pics/ajax-loader.gif',
			});
		});
				setTimeout(function(){$('#seeme').css("display","none")},5000);

});
$('.addPlaceAboveCaretV').each(function() {
    var h = $(this).height();
    var w = $(this).width();
    $(this).find('canvas').attr('height', h*1.1 );
  // no need for, block element $(this).find('canvas').css('margin-left', '-' + w );
    $(this).find('canvas').attr('width', w*1.1 );
    $(this).find('canvas').css('margin-top', '-' + h );
    $('canvas').each(function(){
        var context =  $(this)[0].getContext('2d');
        var startX = w*0.1;
        var startY = 0;
            
        var ctrl1X = w*1.1;
        var ctrl1Y = h*1.1;

        var ctrl2X = 0;
        var ctrl2Y = h*1.1;
        
        var endX =  w + w*0.1;
        var endY = 0;

          
          
        context.beginPath();
        context.moveTo(startX, startY);
        context.bezierCurveTo(ctrl1X, ctrl1Y, ctrl2X, ctrl2Y, endX, endY);
      // line color
      context.strokeStyle = 'grey';
      context.stroke();
});
});

$('.addPlaceAboveCaretBow').each(function() {
    var h = $(this).height();
    var w = $(this).width();
    $(this).find('canvas').attr('height', h*1.1 );
  // no need for, block element $(this).find('canvas').css('margin-left', '-' + w );
    $(this).find('canvas').attr('width', w*1.1 );
    $(this).find('canvas').css('margin-top', '-' + h );
    $('canvas').each(function(){
        var context =  $(this)[0].getContext('2d');
        var startX = w;
        var startY = h*0.5;
            
        var ctrl1X = w*1.1;
        var ctrl1Y = h*0.6;

        var ctrl2X = 0;
        var ctrl2Y = h*0.6;
        
        var endX = w*0.1;
        var endY = h*1.1;

          
          
        context.beginPath();
        context.moveTo(startX, startY);
        context.bezierCurveTo(ctrl1X, ctrl1Y, ctrl2X, ctrl2Y, endX, endY);
      // line color
      context.strokeStyle = 'grey';
      context.stroke();
});
});

$('canvas').each(function(){
        var id = $(this).attr('id');
        var h = $('.col-md-4:first').height();
    $('#can').attr('height', h );
});
$('.fractionlike').each(function(){
        var str1 = $(this).prev().text();
        var replacedstring1 = str1.replace(/\s.{1,2}$/g, " ");
            $(this).prev().text(replacedstring1);
         var str2 = $(this).next().text();
         var repstr2 = $(this).find('td.border-top').text();
         var replacedstring2 = str2.replace(repstr2, "");
             $(this).next().text(replacedstring2);
    });