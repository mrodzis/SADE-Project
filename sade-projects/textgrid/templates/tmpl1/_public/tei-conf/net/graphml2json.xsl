<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="tei xs" version="2.0">
    <xsl:template match="/">
        <xsl:result-document href="c07.json" method="text" media-type="application/json">
            {
            "nodes":[
            <xsl:apply-templates select="//node"/>
            ],
            "links":[
            <xsl:apply-templates select="//edge"/>
            ]
            }
        </xsl:result-document>
    </xsl:template>
    <xsl:template match="node">
        <xsl:variable select="./@id" name="id"/>
        <xsl:if test="not(preceding::*[@id = $id])">
            <xsl:text>{"name":"</xsl:text>
            <xsl:value-of select="./@id"/>
            <xsl:text>","group":</xsl:text>
            <xsl:choose>
                <xsl:when test="./data[@key='type'] eq 'place'">
                    <xsl:text>1}</xsl:text>
                </xsl:when>
                <xsl:when test="./data[@key='type'] eq 'person'">
                    <xsl:text>2}</xsl:text>
                </xsl:when>
                <xsl:when test="./data[@key='type'] eq 'org'">
                    <xsl:text>3}</xsl:text>
                </xsl:when>
                <xsl:when test="./data[@key='type'] eq 'event'">
                    <xsl:text>4}</xsl:text>
                </xsl:when>
                <xsl:when test="./data[@key='type'] eq 'work'">
                    <xsl:text>5}</xsl:text>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="position() != last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
            <xsl:text/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="edge">
        <xsl:variable select="./@source" name="source"/>
        <xsl:variable select="./@target" name="target"/>
        <xsl:if test="not(preceding::edge[(@source eq $source and @target eq $target) or (@source eq $target and @target eq $source)])">
            <xsl:variable name="counter">
                <xsl:choose>
                    <xsl:when test="following::edge[(@source eq $source and @target eq $target) or (@source eq $target and @target eq $source)]">
                        <xsl:value-of select="count(following::edge[(@source eq $source and @target eq $target) or (@source eq $target and @target eq $source)]) + 1"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="1"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:for-each select="//node">
                <xsl:if test="./@id eq $source">
                    <xsl:text>{"source":</xsl:text>
                    <xsl:value-of select="position() - 1"/>
                    <xsl:for-each select="//node">
                        <xsl:if test="(./@id eq $target)">
                            <xsl:text>,"target":</xsl:text>
                            <xsl:value-of select="position() - 1"/>
                            <xsl:text>,"value":</xsl:text>
                            <xsl:value-of select="$counter"/>
                            <xsl:text>}</xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
            <xsl:if test="position() != last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
            <xsl:text/>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>