var pathArray = window.location.pathname.split( '/' );
if ( pathArray[4] != 'textgrid' ){
        $('div#sidebar li a').each(function(){ $(this).attr('href', $(this).attr('href').replace(/textgrid\//g, pathArray[4] + '/'))});
    }

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function highlight(id) {
    var array = id.split(" ");
    for (var i in array) {
        if( array[i].indexOf(":") > 0 ) {
            var target = array[i].replace(":", "");
        }
        else {
            var target = array[i];
        };
        $('#' + target + ', *[data-ref="' + target + '"]' ).toggleClass('fhighlighted');
    }
}
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

$( document ).ready(function() {
    if (getCookie('rs').indexOf('inactive') == -1 && getCookie('rs').length > 0) { $(".rs").toggleClass('rsHigh'); }
    if (getCookie('date').indexOf('inactive') == -1 && getCookie('date').length > 0) { $(".date").toggleClass('dateHigh'); }
    if (getCookie('add').indexOf('inactive') == -1 && getCookie('add').length > 0) { $(".add").toggleClass('addHigh'); }
    if (getCookie('hi').indexOf('inactive') == -1 && getCookie('hi').length > 0) { $(".hi").toggleClass('hiHigh'); }
    if (getCookie('mod').indexOf('inactive') == -1 && getCookie('mod').length > 0) { $(".mod").toggleClass('modHigh'); }
    if (getCookie('del').indexOf('inactive') == -1 && getCookie('del').length > 0) { $(".del").toggleClass('delHigh'); }
    if (getCookie('latn').indexOf('inactive') == -1 && getCookie('latn').length > 0) { $(".Latn").toggleClass('latnHigh'); }
    if (getCookie('script').indexOf('inactive') == -1 && getCookie('script').length > 0) {
        $(".clean").toggleClass('cleanHigh');
        $(".angular").toggleClass('angularHigh');
        $(".hasty").toggleClass('hastyHigh');
    }
    if (getCookie('milestone').indexOf('inactive') == -1 && getCookie('milestone').length > 0) { $(".milestone").toggle(); }
    if (getCookie('ref').indexOf('block') > -1) { $( '.href' ).toggle(); }
    $('#facs').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".surface").toggleClass("surfacefacs");
        $(".facs").toggleClass("visible");
        $(".thumbs").css("margin-left", "24cm");
        // trigger a 10px scroll to top and back again, to activate the lazy loader
        var y = $(window).scrollTop();  //current y position
        // $(window).scrollTop(y-1);
        // $(window).scrollTop(y+1);
    });

    $('#facsBtn').click(function() {
        // true = on
        var status = $( this ).attr('class').indexOf("inactive") < 0;
        if( status === true ){
            if (myLayout.root.getItemsByType("component").length > 1) {
            $( this ).toggleClass( "inactive" );
            goldenDestroy( "facs" );
            }
        }
        else {
            $( this ).toggleClass( "inactive" );
            goldenActivate( "facs" );
        }
    });

    $('#transBtn').click(function() {
        // true = on
        var status = $( this ).attr('class').indexOf("inactive") < 0;
        if( status === true ){
            if (myLayout.root.getItemsByType("component").length > 1) {
            $( this ).toggleClass( "inactive" );
            goldenDestroy( "tran" );
            }
        }
        else {
            $( this ).toggleClass( "inactive" );
            goldenActivate( "tran" );
        }
    });

    $('#xmlBtn').click(function() {
        // true = on
        var status = $( this ).attr('class').indexOf("inactive") < 0;
        if( status === true ){
            if (myLayout.root.getItemsByType("component").length > 1) {
            $( this ).toggleClass( "inactive" );
            goldenDestroy( "code" );
            }
        }
        else {
            $( this ).toggleClass( "inactive" );
            goldenActivate( "code" );
        }
    });
    $('#tocBtn').click(function() {
        // true = on
        var status = $( this ).attr('class').indexOf("inactive") < 0;
        if( status === true ){
            if (myLayout.root.getItemsByType("component").length > 1) {
            $( this ).toggleClass( "inactive" );
            goldenDestroy( "toc" );
            }
        }
        else {
            $( this ).toggleClass( "inactive" );
            goldenActivate( "toc" );
        }
    });


    $('#btns').click(function() {
        // true = on
        if( $('#infoViewer').attr("class").indexOf("inactive") === -1) {
            infoAction();
            btnsAction();
        }
        else { btnsAction(); }
    });

function btnsAction() {
    $( '#btns' ).toggleClass( "inactive" );
    $("#buttons").toggle();
    $('#helperUnderSectionHeader').toggleClass('doubleHelper');
}
function infoAction() {
    $('#infoViewer').toggleClass( "inactive" );
    $("#infoView").toggle();
    $('#helperUnderSectionHeader').toggleClass('doubleHelper');
}
    $('#infoViewer').click(function() {
        // true = on
        if( $('#btns').attr("class").indexOf("inactive") === -1) {
            btnsAction();
            infoAction();
        }
        else { infoAction(); }
    });

    $('#rsBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".rs").toggleClass('rsHigh');
        var status = $( this ).attr('class');
        createCookie('rs',status,180);
    });
    $('#dateBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".date").toggleClass('dateHigh');
        var status = $( this ).attr('class');
        createCookie('date',status,180);
    });
    $('#refBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $( '.href' ).toggle();
        var status = $( '.href' ).css('display');
        createCookie('ref',status,180);
    });
    $('#addBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".add").toggleClass('addHigh');
        var status = $( this ).attr('class');
        createCookie('add',status,180);
    });
    $('#modBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".mod").toggleClass('modHigh');
        var status = $( this ).attr('class');
        createCookie('mod',status,180);
    });
    var $allMods = $("[class*='mod-seq']"), lenMods = $allMods.length, cMods=0;
    $("#seqBtn").click( function(){
      $allMods.removeClass("hidden").eq(++cMods%lenMods).addClass("hidden");
    });
    $('#delBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".del").toggleClass('delHigh');
        var status = $( this ).attr('class');
        createCookie('del',status, 180);
    });
    $('#hiBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".hi").toggleClass('hiHigh');
        var status = $( this ).attr('class');
        createCookie('hi',status, 180);
    });

    $('#rotationBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        // is 0 on init
        var rotationState = myLayout.root.getItemsById("facs")[0].config.componentState.rotation || 0;
        var newsotationState = rotationState + 45;
        rotate(newsotationState);
        myLayout.root.getItemsById("facs")[0].container.extendState({ rotation: newsotationState });
    });

    $('#milestoneBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".milestone").toggle();
        var status = $( this ).attr('class');
        createCookie('milestone',status, 180);
    });
    $('#latnBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".Latn").toggleClass( 'latnHigh' );
        var status = $( this ).attr('class');
        createCookie('latn',status, 180);
    });
    $('#scriptBtn').click(function() {
        $( this ).toggleClass( "inactive" );
        $(".clean").toggleClass( 'cleanHigh' );
        $(".angular").toggleClass( 'angularHigh' );
        $(".hasty").toggleClass( 'hastyHigh' );
        var status = $( this ).attr('class');
        createCookie('script',status, 180);
    });
});

function rotate(deg){
    $(".ffacs > *, .ftran > *").css("transform", "rotate("+deg+"deg)");
    // TODO: add margin left and margin top
    if( deg % 45 === 0 ) {
        $(".ffacs > *, .ftran > *").css("margin-left", "150px");
        $(".ffacs > *, .ftran > *").css("margin-top", "50px");
    }
    if( deg % 90 === 0) {
        $(".ffacs > *, .ftran > *").css("margin-left", "150px");
        $(".ffacs > *, .ftran > *").css("margin-top", "-100px");
    }
    if( deg % 180 === 0 ) {
        $(".ffacs > *, .ftran > *").css("margin-left", "0");
        $(".ffacs > *, .ftran > *").css("margin-top", "0");
    }
}

// Toggle text on figures and sketches
$('.zone.figure').click(function() {
    $( this ).children().toggle();
});
var width=$('.line-through.triple_oblique45deg').width();
$('.triple_oblique1').css('width', width);
$('.triple_oblique2').css('width', width);
$('.triple_oblique3').css('width', width);

var array=window.location.search.replace("?", "").split('&');
$.each( array, function( key, value ) {
  if(value.indexOf('target') == 0 ) {
    var id=value.split('=')[1];
    $('#'+id).css('background-color', 'yellow');
  }
});

$('#RotateSlide').on('change', function(e){
    var rotation = document.getElementById("RotateSlide").value;
    console.log(rotation);
    $('.sourceDoc > div').css('transform-origin', 'center');
    $('.sourceDoc > div').css('transform', 'rotate('+rotation+'deg)');
    $('div.ffacs img').css('transform-origin', 'center');
    $('div.ffacs img').css('transform', 'rotate('+rotation+'deg)');

});


// Search box toggle
// =================
$('#search-btn').on('click', function() {
	$("#search-icon").toggleClass('fa-search fa-times margin-2');
	$("#search-box").toggleClass('show hidden animated fadeInUp');
	return false;
});

// getParameter function available everywhere
// implementation for LitVZ
function getParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
            .split("&")
                .forEach(function (item) {
                    tmp = item.split("=");
                    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
                });
    return result;
}

// lazyload trigger
$('.panel-collapse').on('show.bs.collapse', function () {
    var img = $(this).find("img"),
        orig = img.attr("data-original");
    img.attr("src", orig);
});

// trigger a specific state
var stateTrans = '{"settings":{"hasHeaders":true,"constrainDragToContainer":true,"reorderEnabled":true,"selectionEnabled":false,"popoutWholeStack":false,"blockedPopoutsThrowError":true,"closePopoutsOnUnload":true,"showPopoutIcon":false,"showMaximiseIcon":true,"showCloseIcon":false,"responsiveMode":"onload"},"dimensions":{"borderWidth":5,"minItemHeight":10,"minItemWidth":10,"headerHeight":20,"dragProxyWidth":300,"dragProxyHeight":200},"labels":{"close":"close","maximise":"maximise","minimise":"minimise","popout":"open in new window","popin":"pop in","tabDropdown":"additional tabs"},"content":[{"type":"stack","width":100,"isClosable":true,"reorderEnabled":true,"title":"","activeItemIndex":0,"content":[{"type":"component","componentName":"tran","title":"Transkription","id":"tran","componentState":{"importclass":".ftran"},"width":33,"isClosable":false,"reorderEnabled":true}]}],"isClosable":true,"reorderEnabled":true,"title":"","openPopouts":[],"maximisedItemId":null}';
var stateCode  = '{"settings":{"hasHeaders":true,"constrainDragToContainer":true,"reorderEnabled":true,"selectionEnabled":false,"popoutWholeStack":false,"blockedPopoutsThrowError":true,"closePopoutsOnUnload":true,"showPopoutIcon":false,"showMaximiseIcon":true,"showCloseIcon":false,"responsiveMode":"onload"},"dimensions":{"borderWidth":5,"minItemHeight":10,"minItemWidth":10,"headerHeight":30,"dragProxyWidth":300,"dragProxyHeight":200},"labels":{"close":"close","maximise":"maximise","minimise":"minimise","popout":"open in new window","popin":"pop in","tabDropdown":"additional tabs"},"content":[{"type":"stack","isClosable":true,"reorderEnabled":true,"title":"","width":100,"activeItemIndex":0,"content":[{"type":"component","componentName":"code","title":"Code","id":"code","componentState":{"importclass":".fcode"},"width":30,"isClosable":false,"reorderEnabled":true},{"type":"component","componentName":"toc","title":"Blattübersicht","id":"toc","componentState":{"importclass":".ftoc"},"isClosable":false,"reorderEnabled":true},{"type":"component","componentName":"comm","title":"Stellenkommentar (0)","id":"comm","componentState":{"importclass":".fcomm"},"isClosable":false,"reorderEnabled":true}]}],"isClosable":true,"reorderEnabled":true,"title":"","openPopouts":[],"maximisedItemId":null}';
function goldenState( trigger ){
    switch (trigger) {
        case 'trans':
            localStorage.setItem( 'savedState', stateTrans );
            break;
        case 'code':
            localStorage.setItem( 'savedState', stateCode );
            break;
        default:
            console.log( "unprepared state item called" );
    }
}

// XHR have to reregister
function registerInfo() {
    $('[data-info]').mouseenter(function(){
        $( "div#infoView > .container" ).html('&#160;');
        $('div#infoView > .container').append( $(this).attr('data-info') );
    });
}
