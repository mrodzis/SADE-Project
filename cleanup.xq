xquery version "3.1";
(: call this script after initialization of the db :)

let $login := xmldb:login("/db/sade-projects/textgrid/", "admin", "")

let $configdoc := doc( "/db/sade-projects/textgrid/config.xml" )

let $do := (
    update replace $configdoc//param[@key="textgrid.user"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="textgrid.password"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="secret"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="sade.password"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="dokuwiki.user"]/text() with text {"TODO"},
    update replace $configdoc//param[@key="dokuwiki.password"]/text() with text {"TODO"})

return
    "done."
