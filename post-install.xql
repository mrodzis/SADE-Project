xquery version "3.1";

declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";

import module namespace tgconnect="http://textgrid.info/namespaces/xquery/tgconnect" at "/db/apps/textgrid-connect/tg-connect.xql";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient"   at "/db/apps/textgrid-connect/tgclient.xqm";


(:~
 : Calculates RGB color scale from red to green
 : @param $pos – the current position in the sequence
 : @param $count – the number of steps to prepare
 : @result A string that complies to RGB terminal color code
 : @see https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit
 :   :)
(:declare function local:getColor($pos as xs:int, $count as xs:int)
as xs:string {
let $steps := (510 div $count) => floor()
let $this := $pos * $steps
    return
        if( $this lt 255 )
        then "255;" || $pos * $steps || ";0"
        else 510 - $pos * $steps || ";255;0"
};

declare variable $moveProject := (
    xmldb:move("/db/apps/fontane/sade-projects", "/db"),
    xmldb:move("/db/apps/fontane/system/config/db/sade-projects", "/db/system/config/db"),
    xmldb:move("/db/apps/fontane/system/autostart", "/db/system")
    );

declare variable $preconfigdoc := doc( "/db/sade-projects/textgrid/config.xml" );
declare variable $credentials := file:read("/var/lib/textgrid/fontane/credentials.txt")
                                => tokenize("\n");

declare variable $credentialInjection :=
  for $line at $pos in $credentials
  return
  switch ($pos)
    case 1 return update replace $preconfigdoc//param[@key="textgrid.user"]/text() with text {$line}
    case 2 return update replace $preconfigdoc//param[@key="textgrid.password"]/text() with text {$line}
    case 3 return (
        (update replace $preconfigdoc//param[@key="secret"]/text() with text {$line}),
        (update replace $preconfigdoc//param[@key="sade.password"]/text() with text {$line}))
    case 4 return update replace $preconfigdoc//param[@key="dokuwiki.user"]/text() with text {$line}
    case 5 return update replace $preconfigdoc//param[@key="dokuwiki.password"]/text() with text {$line}
    default return false();

declare variable $configdoc := doc( "/db/sade-projects/textgrid/config.xml" );

declare variable $config := map:new(for $param in $configdoc/config/param
                              return map:entry(string($param/@key), string($param))
                              );

declare variable $sysout := util:log-system-out("webauth: "||$config("textgrid.webauth"));

declare variable $sid :=
  tgclient:getSid(
    $config("textgrid.webauth"),
    $config("textgrid.authZinstance"),
    $config("textgrid.user"),
    $config("textgrid.password")
  );

let $log := util:log-system-out( "&#27;[30;43m initiating post-install.xql &#27;[0m" ):)

(: the uris we need to publish in an intended order! :)
(:let $filters := <filters>
                    <filter key="format" value="text/xml"/>
                    <filter key="project.id" value="TGPR-9b18459d-3a6b-004f-b6aa-4fba2b9e1d3e"/>
                </filters>:)
    (: im Titel nicht "Notizbuch" :)
(:let $log := util:log-system-out( "&#27;[30;43m getting non-notebook XML URIs &#27;[0m" )
let $query := "%21%28title%3A%22Notizbuch%22%29"
let $non-notebook-xmls := tgclient:tgsearch-query-filter($filters, $query, $sid, 2000, 0)
                            //tgmd:textgridUri/substring-before(., ".")
                            => distinct-values()
let $filters := <filters>
                    <filter key="format" value="text/linkeditorlinkedfile"/>
                    <filter key="project.id" value="TGPR-9b18459d-3a6b-004f-b6aa-4fba2b9e1d3e"/>
                </filters>
let $query := ""
let $log := util:log-system-out( "&#27;[30;43m getting TILE object URIs &#27;[0m" )
let $tble-objects := tgclient:tgsearch-query-filter($filters, $query, $sid, 2000, 0)
                            //tgmd:textgridUri/substring-before(., ".")
                            => distinct-values()


let $filters := <filters>
                    <filter key="format" value="text/xml"/>
                    <filter key="project.id" value="TGPR-9b18459d-3a6b-004f-b6aa-4fba2b9e1d3e"/>
                </filters>:)
    (: im Titel "Notizbuch" :)
(:let $query := "%28title%3A%22Notizbuch%22%29"
let $log := util:log-system-out( "&#27;[30;43m getting notebook XML URIs &#27;[0m" )
let $notebooks := tgclient:tgsearch-query-filter($filters, $query, $sid, 2000, 0)
                            //tgmd:textgridUri/substring-before(., ".")
                            => distinct-values()
let $notebooks := ()

let $countNotebooks := count($notebooks)

let $special-files := ("textgrid:342bm",
                        "textgrid:34rhf")

let $installSequence :=  ( $special-files, $tble-objects, $non-notebook-xmls, $notebooks )
let $report := for $uri at $pos in $installSequence
                return
                  update insert (<publish uri="{$uri}"/>) into //initlog/install/todo

let $log := if(count($notebooks) lt 10)
            then util:log-system-out( "&#27;[38;5;210m&#27;[48;5;33m we are on fastdeploy! &#27;[0m" )
            else util:log-system-out( "&#27;[38;5;231m&#27;[48;5;52m complete import… got coffee? &#27;[0m" )
let $count := count( $installSequence ):)

(: there are some documents with an unknown status in the pipeline: :)
(:let $removeSomeObjects := if( $config("sade.develop") = "true" )
                          then ("textgrid:27656", "textgrid:27657", "textgrid:27658", "textgrid:2765b", "textgrid:2765c")
                          else ("textgrid:27656", "textgrid:27657", "textgrid:27658", "textgrid:2765b", "textgrid:2765c",
                                "textgrid:253sx", "textgrid:253t0", "textgrid:253t1", "textgrid:253t2", "textgrid:253t3")

return
( :)
(: PUBLISH :)
(:util:log-system-out( "getting DATA from TextGrid…" ),
    for $uri at $pos in $installSequence[. != $removeSomeObjects]
    where starts-with($uri, "textgrid:")
    let $color := local:getColor($pos, $count)
    let $systemOut := util:log-system-out( "&#27;[48;2;"|| $color ||"m&#27;[38;2;0;0;0m "
                        || $uri
                        || (if ($uri = $notebooks) then (" " || index-of($notebooks, $uri) || "/" || $countNotebooks) else "")
                        || " &#27;[0m" )
    let $startTime := util:system-time()
    let $response :=
      try {
            tgconnect:publish(
              $uri,
              $sid,
              "data",
              "admin",
              "",
              "textgrid",
              "",
              true())
    } catch * { <error code="{$err:code}" timestamp="{current-dateTime()}">{$err:description}</error> }
return
  update insert ( <report uri="{$uri}" started="{$startTime}" finished="{util:system-time()}">{$response}</report>) into //initlog/install/done,
:)
(: INDEX :)
(:util:log-system-out( "reindexing…" ),
        let $doc := doc( "/db/system/config/db/sade-projects/textgrid/data/xml/data/collection.xconf" )/*
        let $tmp := xmldb:store( "/db", "tmp-collection.xconf", $doc )
        let $log := util:log-system-out( "storing temp:" || $tmp )
        let $newdoc := doc( $tmp )

        let $remove := xmldb:remove( "/db/system/config/db/sade-projects/textgrid/data/xml/data", "collection.xconf"),
            $log := util:log-system-out( "removing original…"  )

        let $store := xmldb:store( "/db/system/config/db/sade-projects/textgrid/data/xml/data", "collection.xconf", $newdoc ),
            $log := util:log-system-out( "storing new:" || $store )
        return
            xmldb:remove( "/db", "tmp-collection.xconf" )
    ),

xmldb:reindex("/db/sade-projects/textgrid/data/xml/data"),
xmldb:reindex("/db/sade-projects/textgrid/data/xml/meta"),
xmldb:reindex("/db/sade-projects/textgrid/cache"),

util:log-system-out( "almost done" ):)

(: beacon to end on running gitlab CI :)
(:util:eval("file:serialize(<done/>, '/tmp/sade.beacon', ())"),:)

(: set correct mode for autostart trigger :)
(:sm:chmod(xs:anyURI("/db/system/autostart"), "rwxrwx---"),
sm:chmod(xs:anyURI("/db/system/autostart/reindex.xq"), "rwxrwx---"),
sm:chmod(xs:anyURI("/db/sade-projects/textgrid/data/xml/xq/get.xq"), "rwxr-xr--")
):)
